<div>
<img width="100%" height="93" src=".gitlab/media/logo_gitlab_fullwidth.svg">
</div>

<div align="center">
This repository is used for example wallpaper and widgets. You can contribute to them [here](https://gitlab.com/kelteseth/screenplay-examples)

</div>

### Wallpaper
* [Landscape](wallpaper/landscape/Readme.md)
    * Simple QML Wallpaper that uses abstract landscape image that move 
* [Particles](wallpaper/particles/Readme.md)
    * QML particle system that follows the users mouse
### Widgets
* [Clock](widgets/digital-clock/Readme.md)
    * QML Widget that shows the current time as a digital clock 
* [xkcd](widgets/xkcd/Readme.md)
    * QML Widget that shows the latests [xkcd](https://xkcd.com)