import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import net.aimber.screenplaysdk 1.0

Window {
    id: mainWindow
    visible: true
    width: 250
    height: 100
    color: "transparent"
    flags: Qt.SplashScreen | Qt.ToolTip | Qt.WindowStaysOnBottomHint

    ScreenPlaySDK {
        onIncommingMessage: {
            print(msg)
        }
    }

    Clock {
        id: clock
    }

    MouseArea {
        id: ma
        property point clickPos
        anchors {
            top: parent.top

            right: parent.right
            bottom: parent.bottom
            left: parent.left
        }
        propagateComposedEvents: true
        hoverEnabled: true
        onEntered: {
            clock.state = "hover"
        }
        onExited: {
            clock.state = ""
        }

        onPressed: {
            clickPos = Qt.point(mouse.x, mouse.y)
        }

        onPositionChanged: {
            if (ma.pressed) {
                var delta = Qt.point(mouse.x - clickPos.x, mouse.y - clickPos.y)
                var new_x = mainWindow.x + delta.x
                var new_y = mainWindow.y + delta.y
                if (new_y <= 0)
                    mainWindow.visibility = Window.Maximized
                else {
                    if (mainWindow.visibility === Window.Maximized)
                        mainWindow.visibility = Window.Windowed
                    mainWindow.x = new_x
                    mainWindow.y = new_y
                }
            }
        }
    }
}
