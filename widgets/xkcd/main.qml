import QtQuick 2.9
import QtGraphicalEffects 1.0
import QtQuick.Particles 2.0

Item {
    id: root
    anchors.fill: parent

    signal sizeChanged(var size)

    Image {
        id: img
        anchors.fill: parent
        onSourceChanged: {
            sizeChanged(img.sourceSize)
        }

        fillMode: Image.PreserveAspectCrop
    }

    Component.onCompleted: {
        request("http://xkcd.com/info.0.json", function (o) {
            if (o.status === 200) {
                var d = eval('new Object(' + o.responseText + ')')
                console.log(o.responseText)
                img.source = d.img
            } else {
                console.log("Some error has occurred")
            }
        })
    }

    function request(url, callback) {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = (function (myxhr) {
            return function () {
                if (myxhr.readyState === 4)
                    callback(myxhr)
            }
        })(xhr)
        xhr.open('GET', url)
        xhr.send('')
    }
}
